__version__='0.1'
__date__='2011/08/10'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, csv, argparse, gzip, bz2, zipfile

def parse_args():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Read delimited file and transpose the file. only works for file with same number in each line and column.' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                      epilog=sEpilog)
    parser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))
    parser.add_argument('sFile',
                         metavar='FILE',
                         help='Delimited file. [if set to "-" reads from standard in]')
    parser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sep',
                         default='\t',
                         help='Delimiter used in file.  [default: "tab"]')
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
        
    args = parser.parse_args()
    return args, parser

def load_file(s):
    if s in ['-', 'stdin']:
        oF = sys.stdin
    elif s.split('.')[-1] == 'gz':
        oF = gzip.open(s, 'rt')
    elif s.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(s)
    elif s.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(s)
    else:
        oF = open(s)
    return oF

def main():
    args, parser = parse_args()
    
    oF = load_file(args.sFile)
            
    for t in zip(*list(csv.reader(oF, delimiter=args.sep))):
        sys.stdout.write('{}\n'.format(args.sep.join(t)))
        
if __name__ == '__main__':
    main()

