args = commandArgs(TRUE)

#
# usage:
# R --vanilla --slave --args \
#	inFile outFile \
#	< classify_samples_with_CMSclassifier.R 
#	> classify_samples_with_CMSclassifier.measure.log 2>&1
#
# use CMSclassifier to classify the input file and 
# write the output table into the output file

# function to check package install
library(devtools)

#install_github("Sage-Bionetworks/CMSclassifier", force=TRUE)
#install('./data/CMSclassifier')
pkgTest <- function(x) {
    if (!require(x,character.only = TRUE))
    {
      install('./data/CMSclassifier')
        if(!require(x,character.only = TRUE)) stop("Package not found")
    }
}


pkgTest("CMSclassifier")
library(CMSclassifier)
sessionInfo()

inFile <- as.character(args[1])
outFile <- as.character(args[2])

samples_x_genes_table <- read.table(
	inFile,
	header = TRUE,
	row.names = 1, 
	check.names = FALSE	
)

classified_samples <- CMSclassifier::classifyCMS(
	t(log2(samples_x_genes_table+1)), 
	method="SSP"
)[[3]]

# some magic to get correct header number
write.table(
    data.frame("Samples"=rownames(classified_samples), classified_samples),
    file=outFile,
    append=FALSE,
    quote=FALSE,
    sep="\t",
    row.names=FALSE
)
