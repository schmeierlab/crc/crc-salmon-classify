#!/usr/bin/env python
"""
NAME: seb.FILE.xlsx2tab.py


DESCRIPTION
===========

INSTALLATION
============

USAGE
=====

VERSION HISTORY
===============

0.1.0    2015/01/28    Initial version.

"""
__version__='0.1.0'
__date__='2015/01/28'
__email__='s.schmeier@gmail.com'
__author__='Sebastian Schmeier'
import sys, os, argparse, csv, collections
import gzip, bz2, zipfile

import openpyxl as px

def parse_cmdline():
    
    # parse cmd-line -----------------------------------------------------------
    sDescription = 'Convert a tab/comma-seperated file to an excel xlsx-file.' 
    sVersion='version %s, date %s' %(__version__,__date__)
    sEpilog = 'Copyright %s (%s)' %(__author__, __email__)

    parser = argparse.ArgumentParser(description=sDescription,
                                    epilog=sEpilog)
    parser.add_argument('--version',
                        action='version',
                        version='%s' % (sVersion))

    parser.add_argument('sFile',
                         metavar='INFILE',
                         help='Delimited-file. [if set to "-" or "stdin" reads from standard in]')
    parser.add_argument('sFileXLSX',
                         metavar='OUTFILE',
                         help='xlsx-file to write to.')
    parser.add_argument('-d', '--delimiter',
                         metavar='STRING',
                         dest='sDEL',
                         default='\t',
                         help='Delimiter used in INFILE. [default: "tab"]')
  
    # if no arguments supplied print help
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
        
    args = parser.parse_args()
    return args, parser

def load_file(s):
    if s in ['-', 'stdin']:
        oF = sys.stdin
    elif s.split('.')[-1] == 'gz':
        oF = gzip.open(s, 'rt')
    elif s.split('.')[-1] == 'bz2':
        oF = bz2.BZFile(s)
    elif s.split('.')[-1] == 'zip':
        oF = zipfile.Zipfile(s)
    else:
        oF = open(s)

    return oF

def main():
    args, parser = parse_cmdline()

    wb = px.Workbook(write_only=True)
    ws = wb.create_sheet()
    
    oR = csv.reader(load_file(args.sFile), delimiter = args.sDEL)
    for a in oR:
        ws.append(a)
        
    ## save the file
    wb.save(args.sFileXLSX) 
    
    return
        
if __name__ == '__main__':
    sys.exit(main())
