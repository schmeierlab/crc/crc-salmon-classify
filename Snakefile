import glob, os, os.path
from os.path import join

## Steps:
## 1. Transpose tximport results set  
## 2. CMSclassify
## 3. Attach expression and info

## For data download
#from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
#FTP = FTPRemoteProvider()

## LOAD VARIABLES FROM CONFIGFILE ----------
configfile: "config.yml"

BASEDIR      = config["basedir"]
LOGDIR       = config["logdir"]
BENCHMARKDIR = config["benchmarkdir"]
WRAPPERDIR   = config["wrapperdir"]
SCRIPTDIR    = config["scriptdir"]
ENVS         = config["envs"]

## INPUTS
TXIMPORTFILE = config["tximportfile"]
SAMPLEINFO   =  config["sampleinfo"]

## OUTPUTS
OUTDIR       = config["outdir"]
##------------------------------------------

TARGETS = [join(OUTDIR, 'CMS-classification-results-expr.txt.gz')]
if config['attachinfo']:
    TARGETS.append(join(OUTDIR, 'CMS-classification-results-expr-info.txt.gz'))


## Pseudo-rule to list the final targets,
## so that the whole workflow is run.
rule all:
    input:
        TARGETS


rule transpose:
    input:
        TXIMPORTFILE
    output:
        temp(join(OUTDIR, "input.transposed.txt"))
    log:
        join(LOGDIR, "transpose/transpose.stderr")
    benchmark:
        join(BENCHMARKDIR, 'transpose.txt')
    params:
        script=join(SCRIPTDIR, 'transpose.py')
    shell:
        "python {params.script} {input} > {output} 2> {log}"


rule cmsclassify:
    input:
        join(OUTDIR, "input.transposed.txt")
    output:
        temp(join(OUTDIR, 'CMS-classification-results.txt'))
    benchmark:
        join(BENCHMARKDIR, 'cmsclassify.txt')
    log:
        log1=join(LOGDIR, "cmsclassify/cmsclassify.stdout"),
        log2=join(LOGDIR, "cmsclassify/cmsclassify.stderr")
    conda:
        join(ENVS, "cms.yaml")
    params:
        script = join(SCRIPTDIR, 'classify_samples_with_CMSclassifier.R')
    shell:
        "R --vanilla --slave --args {input} {output} < {params.script} > {log.log1} 2> {log.log2}"


rule attachexpr:
    input:
        expr=join(OUTDIR, "input.transposed.txt"),
        classes=join(OUTDIR, 'CMS-classification-results.txt')
    output:
        join(OUTDIR, 'CMS-classification-results-expr.txt.gz')
    benchmark:
        join(BENCHMARKDIR, 'attachexpr.txt')
    log:
        join(LOGDIR, "attachexpr/attachexpr.stderr")
    params:
        script = join(SCRIPTDIR, 'attach-expr.py')
    shell:
        "join -t $'\t' <(sort {input.classes}) <(sort {input.expr}) 2> {log} | gzip > {output}"


rule attachinfo:
    input:
        cms=join(OUTDIR, 'CMS-classification-results-expr.txt.gz')
    output:
        join(OUTDIR, 'CMS-classification-results-expr-info.txt.gz')
    benchmark:
        join(BENCHMARKDIR, 'attachinfo.txt')
    log:
        log1=join(LOGDIR, "attachinfo/attachinfo.stdout"),
        log2=join(LOGDIR, "attachinfo/attachinfo.stderr")
    params:
        script = join(SCRIPTDIR, 'attach-info.py'),
        info=SAMPLEINFO
    shell:
        "python {params.script} --correct -o {output} {input.cms} {params.info} > {log.log1} 2> {log.log2}"


## rule convertExcel:
##     input:
##         join(OUTDIR, 'CMS-classification-results-expr-info.txt.gz')
##     output:
##         join(OUTDIR, 'CMS-classification-results-expr-info.xlsx')
##     benchmark:
##         join(BENCHMARKDIR, 'convertExcel.txt')
##     log:
##         log1=join(LOGDIR, "convertExcel/convertExcel.stdout"),
##         log2=join(LOGDIR, "convertExcel/convertExcel.stderr")
##     conda:
##         join(ENVS, "xlsx.yaml")
##     params:
##         script = join(SCRIPTDIR, '2xlsx.py')
##     shell:
##         "python {params.script} {input} {output} > {log.log1} 2> {log.log2}"

        
rule clean:
    shell:
        "rm -rf {BASEDIR}/*"
        
        
