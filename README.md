# PROJECT: salmon-classify
AUTHOR: Sebastian Schmeier (s.schmeier@gmail.com) 
DATE: 2017-12-11

## INSTALL


```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
source activate snakemake

# Update conda and packages if you wish
#conda update conda
#conda update --all

# We are using now individual environments per task.
# This is facilitated with snakemake --use-conda
# A rule in the Snakefile now can contain a "conda" directive
# which specifies the conda environemnt to use.
# Conda environments used are stored in envs/

# e.g. to be able to run the example workflow, install
conda install samtools bwa freebayes
```

##  Download CMSclassifier

```bash
cd data; git clone git@github.com:Sage-Bionetworks/CMSclassifier.git; cd -
cd data/CMSclassifier; git remotes remove origin; cd -
```

## RUN SNAKEMAKE-WORKFLOW

```bash
# Do a dryrun of the workflow, show rules, order, and commands
snakemake -np --use-conda

# Run the snakemake workflow, log time and commands 
snakemake -Tp --use-conda 2> run.log

# show a detailed summary of the produced files and used commands
snakemake -D

# To delete all created files use
snakemake -p clean
```

## Create a conda env for a particular tool

We keep the conda envs separate per tool, as to not get problems with
dependencies of different tools. We store yaml-files in directory `envs`.
`Snakemake` will create local conda envs when using `--use-conda` based
on the yaml-files. To create a new yaml-file for a tool do the following:

```bash
conda env create -n snakemake python=3 snakemake
source activate snakemake
conda env export > envs/snakemake.yaml
source deactivate
conda env remove -n snakemake
```

## Updating/testing tools of a conda env

If you wish to update tools in a conda environment in envs/ you need to create
and source the env and update the packages:

```bash
# e.g.
conda env create --name snakemake-test --file envs/snakemake.yaml
source activate snakemake-test
conda update --all --force
# now test it, then:
conda env export > envs/snakemake.yaml
source deactivate
conda env remove -n snakemake-test
```


